## lancelot-user 11 RP1A.200720.011 V12.5.2.0.RJCIDXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: galahad
- Brand: Redmi
- Flavor: lancelot-user
galahad-user
lancelot-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V13.0.2.0.RJCIDXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/lancelot_id/lancelot:11/RP1A.200720.011/V12.5.2.0.RJCIDXM:user/release-keys
- OTA version: 
- Branch: lancelot-user-11-RP1A.200720.011-V12.5.2.0.RJCIDXM-release-keys
- Repo: redmi_galahad_dump_13781


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
